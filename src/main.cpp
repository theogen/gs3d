#include <iostream>
#include <stdexcept>

#include "apploop.hpp"

int main(int argc, const char** argv)
{
	bgfx::RendererType::Enum renderer = bgfx::RendererType::Count;
	if (argc > 1)
	{
		std::string arg(argv[1]);
		if (arg == "vulkan" || arg == "vk")
			renderer = bgfx::RendererType::Vulkan;
		else if (arg == "opengl" || arg == "gl")
			renderer = bgfx::RendererType::OpenGL;
		else if (arg == "opengles" || arg == "essl")
			renderer = bgfx::RendererType::OpenGLES;
		else
		{
			std::cerr << "Unknown renderer type specified via command-line\n";
			return EXIT_FAILURE;
		}
	}

	try {
		tgamefw::AppLoop apploop(renderer);
		apploop.loop();
	}
	catch (const std::exception& e) {
		std::cerr << "Exception: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
