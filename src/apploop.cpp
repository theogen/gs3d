#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>

#include <SDL2/SDL.h>

#include "apploop.hpp"

#include "scene/node.hpp"
#include "scene/camera.hpp"
#include "scene/light.hpp"

namespace tgamefw {

AppLoop::AppLoop(bgfx::RendererType::Enum renderer)

{
	p_display = new video::Display(
		renderer,
		"gs3d",
		1280, 768,
		SDL_WINDOW_RESIZABLE
	);
	p_resource_manager = new ResourceManager();
	p_loader = new Loader(p_resource_manager);
	p_keyboard = new Keyboard();
	p_event_collector = new EventCollector(*p_display, *p_keyboard);
	p_cube_pipeline = new pipeline::Cube(p_display, p_loader);
	p_ship_pipeline = new pipeline::Ship(p_display, p_loader);
}

AppLoop::~AppLoop()
{
	delete p_ship_pipeline;
	delete p_cube_pipeline;
	delete p_keyboard;
	delete p_event_collector;
	delete p_loader;
	delete p_resource_manager;
	delete p_display;
}

void AppLoop::loop()
{
	scene::Node ship("ship");

	scene::Node cube("cube");
	cube.set_position(glm::vec3(8, 0, -5));

	scene::Camera camera("camera");
	camera.set_aspect(p_display->aspect());
	camera.set_fov(60);
	camera.set_near(0.1f);
	camera.set_far(100.0f);
	camera.set_position(glm::vec3(0, 0, -10));
	camera.recalculate_projection_matrix();

	scene::Light light("light");
	light.set_position(glm::vec3(-3, 3, 0));
	light.set_color(glm::vec3(2, 2, 2));

	while (!p_event_collector->is_close_requested())
	{
		// Poll events.
		p_event_collector->poll();

		// Exit on Q or ESC.
		if (p_keyboard->down(Scancode::Q) ||
			p_keyboard->down(Scancode::Escape))
		{
			break;
		}

		if (!p_keyboard->state(Scancode::Space)) {
			ship.rotate(glm::vec3(0, 2, 0), p_display->delta());
		}

		// Update transform.
		if (camera.aspect() != p_display->aspect()) {
			camera.set_aspect(p_display->aspect());
			camera.recalculate_projection_matrix();
			std::cout << "Recalculating projection matrix at frame "
			          << p_display->frame() << ".\n";
			continue;
		}
		auto view_matrix = camera.view_matrix();
		auto proj_matrix = camera.projection_matrix();
		bgfx::setViewTransform(0, &view_matrix, &proj_matrix);

		cube.update();
		ship.update();
		light.update();

		p_display->prepare();

		p_ship_pipeline->render(&camera, {&ship}, &light);
		p_cube_pipeline->render(&camera, {&cube}, &light);

		p_display->update();
	}
}


}
