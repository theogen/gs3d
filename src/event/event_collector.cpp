#include <iostream>
#include <SDL2/SDL.h>

#include "event_collector.hpp"

namespace tgamefw {

EventCollector::EventCollector(video::Display& display, Keyboard& keyboard)
	: p_keyboard(&keyboard), p_display(&display)
{
	p_keyboard_state = SDL_GetKeyboardState(nullptr);
}

void EventCollector::poll()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type) {
			case SDL_QUIT:
				m_quit = true;
				break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				on_key_event(event);
				break;
			case SDL_WINDOWEVENT:
				on_window_event(event);
		}
	}

	// Updating keyboard events.
	uint32_t i = 0;
	for (auto& keyval : p_keyboard->m_key_state) {
		SDL_Scancode code = static_cast<SDL_Scancode>(i);
		keyval.downevent = !keyval.state && p_keyboard_state[code];
		keyval.upevent = keyval.state && !p_keyboard_state[code];
		keyval.state = p_keyboard_state[code];
		i++;
	}

}

// Repeats don't work as intented on some versions of SDL
// so I use keyboard state instead.
void EventCollector::on_key_event(const SDL_Event& event)
{
#if 0
	if (event.key.repeat != 0)
		return;

	//printf("%d %d\n", event.key.state, event.key.repeat);

	KeyCode code = sdl_scancode_to_keycode(event.key.keysym.scancode);
	if (!p_keyboard->m_key_state.count(code))
		p_keyboard->m_key_state[code] = Keyboard::KeyState();

	Keyboard::KeyState* state = &p_keyboard->m_key_state[code];
	if (event.key.type == SDL_KEYDOWN) {
		//printf("Down: %d\n", event.key.repeat);
		state->state = true;
		state->downevent = true;
	}
	else {
		//printf("Up: %d\n", event.key.repeat);
		state->state = false;
		state->upevent = true;
	}
#endif
}

void EventCollector::on_window_event(const SDL_Event& event)
{
	switch (event.window.event) {
		case SDL_WINDOWEVENT_RESIZED:
			p_display->set_size(event.window.data1, event.window.data2);
			break;
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			p_display->set_size(event.window.data1, event.window.data2);
			break;
	}
}

}
