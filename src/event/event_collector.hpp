#pragma once

#include "../input/keyboard.hpp"
#include "../video/display.hpp"

namespace tgamefw {

class EventCollector {
public:
	EventCollector(video::Display& display, Keyboard& keyboard);

	void poll();

	bool is_close_requested() const { return m_quit; }

private:
	void on_key_event(const SDL_Event& event);
	void on_window_event(const SDL_Event& event);

private:
	bool m_quit = false;
	Keyboard* p_keyboard;
	video::Display* p_display;
	const uint8_t* p_keyboard_state;
};

}
