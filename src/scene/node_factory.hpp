#pragma once

#include <string>
#include <unordered_map>

namespace tgamefw {
namespace scene {

class Node;

class NodeBaseCreator {
public:
	virtual ~NodeBaseCreator() = default;
	virtual Node* create() = 0;
};

class NodeFactory {
public:
	static Node* create(const std::string& type)
	{
		if (NodeFactory::has_type(type))
			return NodeFactory::m_creators[type]->create();
		return nullptr;
	}

	static void register_type(const std::string& type, NodeBaseCreator* creator)
	{
		NodeFactory::m_creators[type] = creator;
		//NodeFactory::m_creators.insert(std::pair<String, NodeBaseCreator*>(type, creator));
	}

	static bool has_type(const std::string& type)
	{
		return NodeFactory::m_creators.count(type);
	}

private:
	static std::unordered_map<std::string, NodeBaseCreator*> m_creators;
};

// Macro which defines new creator which adds itself to the factory.
// Must be used when defining new node type in order to use creator on it.
#define REGISTER_NODE_TYPE(CLASS) \
class CLASS##Creator : public NodeBaseCreator { \
public: \
	CLASS##Creator() \
	{ \
		NodeFactory::register_type(#CLASS, this); \
	} \
\
	virtual Node* create() override \
	{ \
		return new CLASS(); \
	} \
}; \
static CLASS##Creator global_##CLASS##Creator;

}}
