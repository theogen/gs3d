#include <unordered_map>

#include "node.hpp"
#include "scene/node_factory.hpp"

using namespace tgamefw::loader::data;

namespace tgamefw {
namespace scene {

/* Factory */

std::unordered_map<std::string, NodeBaseCreator*> NodeFactory::m_creators;

REGISTER_NODE_TYPE(Node)

/* Constructors */

Node::Node(const DataNode& data)
{
	load(data);
}

Node::Node(const DataNode& data, Node& parent)
{
	set_parent(parent);
	load(data);
}

// Initialize by name.
Node::Node(const std::string& name) : m_name(name) {}

// Initialize by name and either parent or position.
Node::Node(const std::string& name, Node& parent)
	: m_name(name), p_parent(&parent) {}
Node::Node(const std::string& name, const glm::vec3& position)
	: m_name(name), m_position(position) {}

// Initialize by name, parent and position.
Node::Node(const std::string& name, Node& parent, const glm::vec3& position)
	: m_name(name), p_parent(&parent), m_position(position) {}
Node::Node(const std::string& name, const glm::vec3& position, Node& parent)
	: m_name(name), p_parent(&parent), m_position(position) {}


/* Loading */

void Node::load(const DataNode& data)
{
	m_name = data.token(1, m_name);

	std::string full = full_name();
#if 0
	glog.log(LOC_,
		"Loading node `" + full + "'",
		LogLevel::debug);
#endif

	for (const DataNode& node : data) {
		if (node.token(0) == "transform") {
			const DataNode& transform = data["transform"];
			m_position.x = transform["position"].value(1);
			m_position.y = transform["position"].value(2);
			m_position.z = transform["position"].value(3);

			m_rotation.x = transform["rotation"].value(1);
			m_rotation.y = transform["rotation"].value(2);
			m_rotation.z = transform["rotation"].value(3);

			m_scale.x = transform["scale"].value(1, m_scale.x);
			m_scale.y = transform["scale"].value(2, m_scale.y);
			m_scale.z = transform["scale"].value(3, m_scale.z);
		}

		if (node.token(0) == "node") {
			std::string type = node.token(2, "Node");

			if (!NodeFactory::has_type(type)) {
				//glog.log(LOC_, "Unknown node type `" + type + "'", LogLevel::error);
				type = "Node";
			}

			Node* n = NodeFactory::create(type);
			n->set_parent(*this);
			n->load(node);
			m_children.push_back(n);
		}
	}

	m_active = data["active"].value(1);
}

std::string Node::full_name() const
{
	const Node* node = p_parent;
	std::string res = m_name;
	while (node != nullptr)
	{
		res = node->m_name + "/" + res;
		node = node->p_parent;
	}

	return res;
}

void Node::update()
{
	for (Node* child : m_children)
		child->update();
}

/* Hierarchy */

void Node::set_parent(Node& node)
{
	// Removing this node from parent's list.
	if (p_parent) {
		auto it = p_parent->m_children.begin();
		auto end = p_parent->m_children.end();
		while (it != end) {
			if (*it != this)
				++it;
			p_parent->m_children.erase(it);
			break;
		}
	}

	// Assigning new parent.
	p_parent = &node;
	p_parent->m_children.push_back(this);
}

void Node::clear(Node* node)
{
	if (!node)
		return;
	for (Node* child : node->m_children) {
		if (!child)
			continue;
		//child->clear(child);
		delete child;
	}
	node->m_children.clear();
}

}}
