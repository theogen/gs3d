#pragma once

#include <list>
#include <string>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../loader/data/data_node.hpp"

namespace tgamefw {
namespace scene {

// Base game node type. Scenes consist of hierarcy of such nodes.
class Node {
public:
	/* Constructors */

	// Default constructor.
	Node() {}

	// Initialize from DataNode.
	Node(const loader::data::DataNode& data);
	Node(const loader::data::DataNode& data, Node& parent);

	// Initialize by name.
	Node(const std::string& name);

	// Initialize by name and either parent or position.
	Node(const std::string& name, Node& parent);
	Node(const std::string& name, const glm::vec3& position);

	// Initialize by name, parent and position.
	Node(const std::string& name, Node& parent, const glm::vec3& position);
	Node(const std::string& name, const glm::vec3& position, Node& parent);

	// Destructor.
	virtual ~Node() {}


	/* Loading */

	// Load node from DataNode.
	virtual void load(const loader::data::DataNode& data);


	/* Node info */

	// Name of this node.
	inline std::string name() const { return m_name; }
	inline void set_name(const std::string& name) { m_name = name; }

	// Returns full path of this node from root.
	std::string full_name() const;

	// Is this node root?
	inline bool is_root() const { return p_parent == nullptr; }

	// State of this individual node.
	inline bool active_self() const { return m_active; }
	inline void set_active(bool state) { m_active = state; }

	// Is this node active overall?
	bool active_global() const { return m_active; }


	/* Transform */

	glm::mat4 transform_matrix() const
	{
		glm::mat4 transform(1);
		transform = glm::translate(transform, m_position);
		transform = glm::rotate(transform, glm::radians(m_rotation.x), glm::vec3(1, 0, 0));
		transform = glm::rotate(transform, glm::radians(m_rotation.y), glm::vec3(0, 1, 0));
		transform = glm::rotate(transform, glm::radians(m_rotation.z), glm::vec3(0, 0, 1));
		transform = glm::scale(transform, m_scale);
		return transform;
	}

	// Position
	inline glm::vec3 position() const { return m_position; }
	inline void set_position(const glm::vec3& pos) { m_position = pos; }

	glm::vec3 position_global() const;
	void set_position_global(const glm::vec3& pos);

	void translate(const glm::vec3& offset, float delta);

	// Rotation
	inline glm::vec3 rotation() const { return m_rotation; }
	inline void set_rotation(const glm::vec3& rot) { m_rotation = rot; }

	glm::vec3 rotation_global() const;
	void set_rotation_global(const glm::vec3& rot);

	void rotate(const glm::vec3& offset, float delta)
	{
		m_rotation += offset * delta;
	}

	// Scale
	inline glm::vec3 scale() const { return m_scale; }
	inline void set_scale(const glm::vec3& scale) { m_scale = scale; }

	glm::vec3 scale_global() const;
	void set_scale_global(const glm::vec3& scale);


	/* Hierarchy */

	// Parent of this node;
	inline Node* parent() const { return p_parent; }
	void set_parent(Node& node);

	// Number of children.
	inline int child_count() const { return m_children.size(); }

	// Creates a child node.
	template <class T>
	T* create(const std::string& name) const;

	// Get child node by index.
	template <class T>
	T* child(unsigned index) const;
	// Get child node by name.
	template <class T>
	T* child(const std::string& name) const;

	// Get sibling node by index.
	template <class T>
	T* sibling(unsigned index) const;
	// Get sibling node by name.
	template <class T>
	T* sibling(const std::string& name) const;

	// Iterator functions for iteration through children of this node.
	std::list<Node*>::const_iterator begin() const { return m_children.begin(); }
	std::list<Node*>::const_iterator end() const { return m_children.end(); }


	/* Update */

	// Update this node.
	virtual void update();

private:
	// Deletes all children nodes.
	void clear(Node* node);

protected:
	// Name of this node.
	std::string m_name = "unnamed";

	// Whether this node is active by itself.
	bool m_active = true;

	// Parent of this node.
	Node* p_parent = nullptr;
	// Children of this node.
	std::list<Node*> m_children;

	// Local position of this node.
	glm::vec3 m_position = glm::vec3(0, 0, 0);
	// Local rotation of this node.
	glm::vec3 m_rotation = glm::vec3(0, 0, 0);
	// Local scale of this node.
	glm::vec3 m_scale = glm::vec3(1, 1, 1);
};

// Creates a child node.
template <class T>
T* Node::create(const std::string& name) const
{
	T* node = new T();
	node->set_parent(*this);
	return node;
}

// Get child node by index.
template <class T>
T* Node::child(unsigned index) const
{
	size_t i = 0;
	for (Node* child : m_children)
		if (i++ == index)
			return dynamic_cast<T>(child);
	return nullptr;
}

// Get child node by name.
template <class T>
T* Node::child(const std::string& name) const
{
	for (Node* child : m_children)
		if (name == child->m_name)
			return dynamic_cast<T>(child);
	return nullptr;
}

// Get sibling node by index.
template <class T>
T* Node::sibling(unsigned index) const
{
	if (!p_parent)
		return nullptr;
	return p_parent->child<T>(index);
}

// Get sibling node by name.
template <class T>
T* Node::sibling(const std::string& name) const
{
	if (!p_parent)
		return nullptr;
	return p_parent->child<T>(name);
}

}}
