#pragma once

#include "node.hpp"

namespace tgamefw {
namespace scene {

class Light : public Node {
public:
	Light() : Node() {}
	Light(const std::string name) : Node(name) {}

	inline glm::vec3 color() const noexcept { return m_color; }
	inline void set_color(glm::vec3 color) noexcept { m_color = color; }

	virtual void update() override {}

private:
	glm::vec3 m_color = glm::vec3(1, 1, 1);
};

}}
