#include "keyboard.hpp"

namespace tgamefw {

Keyboard::Keyboard()
{
	SDL_GetKeyboardState(&m_keys_count);
	m_key_state.resize(m_keys_count);
	for (int i = 0; i < m_keys_count; ++i)
	{
		m_key_state[i] = KeyState();
	}
}

bool Keyboard::state(Scancode key) const
{
	return m_key_state[static_cast<int>(key)].state;
}

bool Keyboard::down(Scancode key) const
{
	return m_key_state[static_cast<int>(key)].downevent;
}

bool Keyboard::up(Scancode key) const
{
	return m_key_state[static_cast<int>(key)].upevent;
}

}
