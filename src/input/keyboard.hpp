#pragma once

#include <SDL2/SDL.h>
#include <vector>
#include "scancode.hpp"

namespace tgamefw {

class EventCollector;

class Keyboard {
private:
	struct KeyState {
		bool state, upevent, downevent;

		KeyState() : state(false), upevent(false), downevent(false) {}
	};

public:
	Keyboard();

	bool state(Scancode key) const;
	bool down(Scancode key) const;
	bool up(Scancode key) const;

private:
	friend class EventCollector;

	int m_keys_count;
	std::vector<KeyState> m_key_state;
};

}
