#pragma once

#include <vector>
#include <bgfx/bgfx.h>
#include "display.hpp"
#include "../loader/loader.hpp"
#include "../scene/node.hpp"
#include "../scene/camera.hpp"
#include "../scene/light.hpp"

namespace tgamefw {
namespace video {

class Pipeline {
public:
	Pipeline(const Display* display, Loader* loader);
	virtual ~Pipeline() {}

	virtual void render(scene::Camera* camera, std::vector<scene::Node*> nodes, scene::Light* light) = 0;

protected:
	static std::string get_shader_path();
	bgfx::ShaderHandle load_shader(const std::string& pipeline, const std::string& name);

protected:
	const Display* p_display;
	Loader* p_loader;
};

}}
