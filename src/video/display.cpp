#include <stdexcept>

#include <SDL2/SDL_syswm.h>

#include <bgfx/platform.h>

#include "display.hpp"

namespace tgamefw {
namespace video {

Display::Display(
	bgfx::RendererType::Enum renderer,
	const std::string& title,
	int width,
	int height,
	uint32_t flags
) :
	m_title(title),
	m_width(width),
	m_height(height),
	m_window_flags(flags)
{
	if (0 != SDL_Init(SDL_INIT_VIDEO)) {
		throw std::runtime_error(std::string("Couldn't initialize SDL: ") + SDL_GetError());
	}

	p_window = SDL_CreateWindow(
		"gs3d",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		m_width, m_height,
		m_window_flags
	);

	if (!p_window) {
		throw std::runtime_error(std::string("Couldn't create window: ") + SDL_GetError());
	}

	// Collect information about the window from SDL.
	SDL_SysWMinfo wmi;
	SDL_VERSION(&wmi.version);
	if (!SDL_GetWindowWMInfo(p_window, &wmi)) {
		SDL_DestroyWindow(p_window);
		throw std::runtime_error(std::string("Couldn't get window info: ") + SDL_GetError());
	}

	// And give the pointer to the window to pd.
	bgfx::PlatformData pd;
	pd.ndt = wmi.info.x11.display;
	pd.nwh = (void*)(uintptr_t)wmi.info.x11.window;

	// Tell bgfx about the platform and window.
	bgfx::setPlatformData(pd);

	// Render an empty frame.
	// This disables render thread.
	bgfx::renderFrame();

	m_reset = BGFX_RESET_NONE | BGFX_RESET_MSAA_X16;
	
	// Initialize bgfx.
	bgfx::Init init;
	init.type = renderer;
	init.vendorId = BGFX_PCI_ID_NVIDIA;
	init.resolution.width = m_width;
	init.resolution.height = m_height;
	init.resolution.reset = m_reset;
	bgfx::init(init);

	// Enable debug text.
	bgfx::setDebug(BGFX_DEBUG_TEXT | BGFX_DEBUG_STATS);

	// Clear the view rect.
	bgfx::setViewClear(
		0,
		BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH,
		0x000000ff,
		1.0f,
		0
	);

	// Set view rectangle for 0th view.
	bgfx::setViewRect(0, 0, 0, uint16_t(m_width), uint16_t(m_height));
}

Display::~Display()
{
	bgfx::shutdown();
	SDL_DestroyWindow(p_window);
	SDL_Quit();
}

void Display::set_size(int width, int height)
{
	m_width = width;
	m_height = height;

	// Recreate swapchain.
	bgfx::reset(m_width, m_height, m_reset);

	// Set view 0 default viewport.
	bgfx::setViewRect(
		0, 0, 0,
		uint16_t(m_width),
		uint16_t(m_height)
	);
}

void Display::prepare()
{
	// Updating time.
	m_ticks = SDL_GetTicks();
	m_last = m_time;
	m_time = static_cast<double>(m_ticks) / 100.0;
	m_delta = m_time - m_last;

	// Set empty primitive on screen.
	bgfx::touch(0);
}

void Display::update()
{
	bgfx::frame();
	++m_frame;
}

}}
