#include "pipeline.hpp"

namespace tgamefw {
namespace video {

Pipeline::Pipeline(const Display* display, Loader* loader)
	: p_display(display), p_loader(loader)
{
}

std::string Pipeline::get_shader_path()
{
	switch (bgfx::getRendererType()) {
		case bgfx::RendererType::Noop:
		case bgfx::RendererType::Direct3D9:  return "/shaders/dx9/";
		case bgfx::RendererType::Direct3D11:
		case bgfx::RendererType::Direct3D12: return "/shaders/dx11/";
		case bgfx::RendererType::Gnm:        return "/shaders/pssl/";
		case bgfx::RendererType::Metal:      return "/shaders/metal/";
		case bgfx::RendererType::Nvn:        return "/shaders/nvn/";
		case bgfx::RendererType::OpenGLES:   return "/shaders/essl/";
		case bgfx::RendererType::OpenGL:     return "/shaders/glsl/";
		case bgfx::RendererType::Vulkan:     return "/shaders/spirv/";

		case bgfx::RendererType::Count:      break;
	}

	throw std::runtime_error("Failed to retrieve shader path");
}

bgfx::ShaderHandle Pipeline::load_shader(const std::string& pipeline, const std::string& name) {
	Resource res = p_loader->resource_manager()->load(
		std::string("testproj/shaders/") + pipeline +
		get_shader_path() + name
	);
	const bgfx::Memory* mem = bgfx::copy(res.data().data(), res.size() + 1);
	mem->data[mem->size - 1] = '\0';
	bgfx::ShaderHandle handle = bgfx::createShader(mem);
	bgfx::setName(handle, name.c_str());
	return handle;
}

}}
