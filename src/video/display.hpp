#pragma once

#include <string>
#include <bgfx/bgfx.h>
#include <SDL2/SDL.h>

namespace tgamefw {

class EventCollector;

namespace video {

class Display {
public:
	Display(
		bgfx::RendererType::Enum renderer,
		const std::string& title,
		int width,
		int height,
		uint32_t flags
	);

	~Display();

	void prepare();
	void update();

	inline int width() const noexcept
	{ return m_width; }

	inline int height() const noexcept
	{ return m_height; }

	inline float aspect() const noexcept
	{ return m_width / static_cast<float>(m_height); } 

	inline unsigned ticks() const noexcept
	{ return m_ticks; }

	inline double time() const noexcept
	{ return m_time; }

	inline double delta() const noexcept
	{ return m_delta; }

	inline long long frame() const noexcept
	{ return m_frame; }

private:
	void set_size(int width, int height);

private:
	friend class ::tgamefw::EventCollector;

	std::string m_title;
	int m_width;
	int m_height;
	uint32_t m_window_flags;
	uint32_t m_reset;

	// Time.
	unsigned m_ticks = 0;
	double m_last = 0;
	double m_time = 0;
	double m_delta = 0;

	long long m_frame = 0;

	SDL_Window* p_window;
};

}}
