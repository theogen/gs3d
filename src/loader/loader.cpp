#include <iostream>
#include <stdexcept>
#include <SDL2/SDL_image.h>
#include "loader.hpp"

namespace tgamefw {

Loader::Loader(ResourceManager* res_mngr)
	: p_res_mngr(res_mngr), m_obj_loader(p_res_mngr) {}

ImageResource Loader::load_image(const std::string& path)
{
	ImageResource res;
	// Loading image.
	res.surface = IMG_Load(path.c_str());
	if (!res.surface) {
		throw std::runtime_error(
			std::string("Couldn't load the texture: ") + IMG_GetError());
	}

	res.width  = res.surface->w;
	res.height = res.surface->h;
	res.pixels = res.surface->pixels;
	res.format = res.surface->format->format;
	res.size   = res.surface->pitch * res.height;

	return res;
}

void Loader::free_image(const ImageResource& image)
{
	SDL_FreeSurface(image.surface);
}

OBJModel Loader::load_obj_model(const std::string& path, const std::string& mat_path)
{
	return m_obj_loader.load(path, mat_path);
}

}
