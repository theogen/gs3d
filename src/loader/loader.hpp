#pragma once

#include <string>
#include <SDL2/SDL.h>

#include "obj_loader.hpp"
#include "resource_manager.hpp"

namespace tgamefw {

struct ImageResource {
	SDL_Surface* surface;
	uint32_t width, height;
	void* pixels;
	uint32_t format;
	unsigned size;
};

class Loader {
public:
	Loader(ResourceManager* res_mngr);

	ImageResource load_image(const std::string& path);
	void free_image(const ImageResource& image);

	OBJModel load_obj_model(const std::string& path, const std::string& mat_path = "");

public:
	ResourceManager* resource_manager() const noexcept
	{ return p_res_mngr; }

private:
	ResourceManager* p_res_mngr;
	OBJLoader m_obj_loader;
};

}
