#pragma once

#include <string>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

#include "tinyobjloader/tiny_obj_loader.h"
#include "resource_manager.hpp"

namespace tgamefw {

struct OBJVertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 texcoord;
	glm::vec3 color;
	float material;

	bool operator==(const OBJVertex& other) const {
		return
			pos == other.pos &&
			normal == other.normal &&
			texcoord == other.texcoord &&
			color == other.color &&
			material == other.material;
	}
};

struct OBJModel {
	std::vector<uint32_t> indices;
	std::vector<OBJVertex> vertices;
	std::vector<tinyobj::material_t> materials;
};

class OBJLoader {
public:
	OBJLoader(ResourceManager* manager) : p_res_mngr(manager) {}

	OBJModel load(const std::string& path, const std::string& mat_path = "");

private:
	OBJVertex create_vertex(tinyobj::attrib_t attrib, tinyobj::index_t index);

	void print_debug_info(const OBJModel& model);

private:
	ResourceManager* p_res_mngr;
};

}
