#pragma once

#include <string>
#include <vector>
#include <streambuf>

namespace tgamefw {

class ResourceStreamBuf : public std::basic_streambuf<char, std::char_traits<char>> {
public:
    ResourceStreamBuf(std::vector<char>& vec)
	{
        setg(vec.data(), vec.data(), vec.data() + vec.size());
    }
};

class ResourceManager;

struct Resource {
	Resource() {}
	Resource(size_t size) : m_data(size) {}

	const std::vector<char>& data() { return m_data; }
	const uint64_t size() { return m_data.size(); }
	ResourceStreamBuf streambuf() { return ResourceStreamBuf(m_data); }

private:
	friend ResourceManager;
	std::vector<char> m_data;
};

class ResourceManager {
public:
	Resource load(std::string path);
};

}
