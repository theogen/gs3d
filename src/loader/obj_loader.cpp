#include <set>
#include <unordered_map>
#include <iostream>

#include "obj_loader.hpp"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tinyobjloader/tiny_obj_loader.h"

template <class T>
inline void hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

namespace std {
    template<>
	struct hash<tgamefw::OBJVertex> {
		size_t operator()(tgamefw::OBJVertex const& vertex) const
		{
			size_t hash = 0;
			hash_combine<glm::vec3>(hash, vertex.pos);
			hash_combine<glm::vec3>(hash, vertex.normal);
			hash_combine<glm::vec2>(hash, vertex.texcoord);
			hash_combine<glm::vec3>(hash, vertex.color);
			hash_combine<float>(hash, vertex.material);
#if 0
			size_t h1 = hash<glm::vec3>()(vertex.pos);
			size_t h2 = hash<glm::vec3>()(vertex.normal);
			size_t h3 = hash<glm::vec2>()(vertex.texcoord);
			size_t h4 = hash<glm::vec3>()(vertex.color);
			size_t h5 = hash<uint32_t>()(vertex.material);
			return ((h1 ^ (h2 << 1)) >> 1) ^ (h3 << 1);
#endif
			return hash;
		}
    };
}

namespace tgamefw {

OBJModel OBJLoader::load(const std::string& path, const std::string& mat_path)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	Resource obj_res = p_res_mngr->load(path);
	Resource mat_res;
	ResourceStreamBuf obj_streambuf = obj_res.streambuf();
	std::istream obj_stream(&obj_streambuf);

	std::cout << "Material path: " << mat_path << '\n';
	tinyobj::MaterialStreamReader* mat_reader = nullptr;
	bool has_material = mat_path != "";
	if (has_material) {
		mat_res = p_res_mngr->load(mat_path);
		ResourceStreamBuf mat_streambuf = mat_res.streambuf();
		std::istream mat_stream(&mat_streambuf);
		mat_reader = new tinyobj::MaterialStreamReader(mat_stream);
	}

	bool success = tinyobj::LoadObj(
		&attrib,
		&shapes,
		&materials,
		&warn, &err,
#if 1
		&obj_stream,
		mat_reader
#else
		path.c_str(),
		"model"
#endif
	);

	if (!warn.empty())
		std::cerr << warn;
	if (!err.empty())
		std::cerr << err;
	if (!success)
		throw std::runtime_error("Failed to load model " + path);
	
	std::unordered_map<OBJVertex, uint32_t> unique_vertices = {};

	OBJModel model;
	for (const auto& shape : shapes) {
		// Loop over faces.
		size_t index_offset = 0;
		for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); ++f) {
			uint8_t fv = shape.mesh.num_face_vertices[f];
			int material_id = shape.mesh.material_ids[f];
			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				auto index = shape.mesh.indices[index_offset + v];
				auto vertex = create_vertex(attrib, index);

				// Per-face material. But it needs to be stored per-vertex.
				vertex.material = material_id;

#if 0
				// Add this vertex only if it's unique.
				if (unique_vertices.count(vertex) == 0) {
					unique_vertices[vertex] = model.vertices.size();
					model.vertices.push_back(vertex);
				}

				model.indices.push_back(unique_vertices[vertex]);
#else
				model.vertices.push_back(vertex);
				model.indices.push_back(model.indices.size());
#endif
			}
			index_offset += fv;
		}
	}


	if (has_material) {
		delete mat_reader;
		model.materials = materials;
	}

	print_debug_info(model);

	return model;
}

OBJVertex OBJLoader::create_vertex(tinyobj::attrib_t attrib, tinyobj::index_t index)
{
	OBJVertex vertex = {};

	vertex.pos = {
		attrib.vertices[3 * index.vertex_index + 0],
		attrib.vertices[3 * index.vertex_index + 1],
		attrib.vertices[3 * index.vertex_index + 2]
	};

	if (attrib.normals.size() != 0)
	{
		vertex.normal = {
			attrib.normals[3 * index.normal_index + 0],
			attrib.normals[3 * index.normal_index + 1],
			attrib.normals[3 * index.normal_index + 2]
		};
	}

	if (attrib.texcoords.size() != 0)
	{
		vertex.texcoord = {
			attrib.texcoords[2 * index.texcoord_index + 0],
			1.0 - attrib.texcoords[2 * index.texcoord_index + 1]
		};
	}

	if (attrib.colors.size() != 0)
	{
		vertex.color = {
			attrib.colors[3 * index.vertex_index + 0],
			attrib.colors[3 * index.vertex_index + 1],
			attrib.colors[3 * index.vertex_index + 2]
		};
	}

	return vertex;
}

void OBJLoader::print_debug_info(const OBJModel& model)
{
	for (const auto& material : model.materials) {
		std::cout
			<< material.name << " "
			<< material.diffuse[0] << " "
			<< material.diffuse[1] << " "
			<< material.diffuse[2] << '\n';
	}

	std::set<int> materials_used;

	for (const OBJVertex& vertex : model.vertices)
		if (materials_used.find(vertex.material) == materials_used.end())
			materials_used.insert(vertex.material);

	for (int id : materials_used)
		std::cout << "Uses material #" << id << '\n';
}

}
