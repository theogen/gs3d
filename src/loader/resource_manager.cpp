#include <fstream>
#include <stdexcept>

#include "resource_manager.hpp"

namespace tgamefw {

Resource ResourceManager::load(std::string path)
{
	static const std::string prefix = "res://";
	if (!path.compare(0, prefix.size(), prefix))
		path = path.substr(prefix.size());

	std::ifstream file(path, std::ios::in | std::ios::ate);
	if (!file.is_open())
		throw std::runtime_error("Failed to load resource: " + path);

	size_t size = file.tellg();
	Resource res(size);

	file.seekg(0);
	file.read(res.m_data.data(), size);

	file.close();

	return res;
}

}
