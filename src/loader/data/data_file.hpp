#pragma once

#include <istream>
#include <list>
#include <vector>
#include <string>

#include "data_node.hpp"

/*
 * Data file format fully inspired by one used in Michael Zahniser's game
 * "Endless Sky". I loved it, so I decided to use it in my game engine. :)
 *
 * https://github.com/endless-sky
 *
 * Each line of the file that is not empty or a comment is a "node" and the
 * relationship between the nodes is determined by indentation: if a node is
 * more indented than the node before it, it is a "child" of that node.
 * Otherwise, it is a "sibling." Each node is just a collection of one or more
 * tokens that can be interpreted either as strings or as floating point
 * values.
 */

namespace tgamefw {
namespace loader {
namespace data {

class DataFile {
public:
	// Load from a file located at specific path.
	bool load(const std::string& path);
	// Load from a file or cin.
	bool load(std::istream& data);

	// Parse from a null-terminated string.
	void parse(const char* data);
	// Parse from a memory region.
	void parse(const char* it, const char* end);

	// DataFile can be passed to functions which take DataNode.
	operator const DataNode&() const { return m_root; }

	// Wrapper of m_root's iterator functions. 
	std::list<DataNode>::const_iterator begin() const;
	std::list<DataNode>::const_iterator end() const;

	// Print content of this file.
	void print_contents() const;
private:
	// Recursive function which parses a level.
	void parse(const char** it, const char* end, int level, DataNode& parent);
	// Get node tokens.
	inline bool parse_node(const char** it, const char *end, DataNode& node);

	// Prints all nodes recursively.
	void print_contents(const DataNode& node, int level) const;

	// Completely clears this instance.
	void clear();
private:
	// Parent node for all nodes.
	DataNode m_root;
	// Filename (for debugging).
	std::string m_filename;
};

}}}
