#pragma once

#include <iostream>

#include <list>
#include <vector>
#include <string>

/*
 * Data file format fully inspired by one used in Michael Zahniser's game
 * "Endless Sky". I loved both the game and the format, so I decided to use it
 * in my engine. :)
 *
 * https://github.com/endless-sky
 *
 * Each line of the file that is not empty or a comment is a "node" and the
 * relationship between the nodes is determined by indentation: if a node is
 * more indented than the node before it, it is a "child" of that node.
 * Otherwise, it is a "sibling." Each node is just a collection of one or more
 * tokens that can be interpreted either as strings or as floating point
 * values.
 */

namespace tgamefw {
namespace loader {
namespace data {

class DataNode {
public:
	explicit DataNode(const DataNode* parent = nullptr);
	DataNode(const DataNode& other);
	DataNode& operator=(const DataNode& other);

	// Check if this node is a dummy.
	bool is_dummy() const { return this == &DataNode::m_dummy; }

	/* Tokens */

	// Get number of tokens in this node.
	size_t size() const;
	// Get token at given index. Returns fallback if index is out of bounds.
	const std::string& token(int index, const std::string& fallback = "") const;
	// Get number at given index. Returns fallback if index is out of bounds.
	double value(int index, double fallback = 0) const;
	// Check if a token can be interpreted as number.
	bool is_num(int index) const;

	/* Children */

	// Whether this node has any children.
	bool has_children() const;

	// Returns first child node with corresponding first token.
	// If no such child exists, returns dummy node.
	const DataNode& operator[](const std::string& first_token) const;

	// Iterator functions
	std::list<DataNode>::const_iterator begin() const;
	std::list<DataNode>::const_iterator end() const;

	/* Debugging */

	// Print trace of this node. You can optionally specify glog log level.
	// void print_trace(int token = -1, debug::LogLevel level = debug::LogLevel::info) const;
private:
	// Children of this node.
	std::list<DataNode> m_children;
	// Tokens of this node.
	std::vector<std::string> m_tokens;

	// Dummy node.
	// This node always fallbacks on token() or value()
	// and returns itself in operator[].
	static const DataNode m_dummy;

	// Cached last child accessed via operator[].
	// Needs to be mutable in order to keep operator[] constant.
	mutable const DataNode* last_child = nullptr;

	// Parent of this node.
	const DataNode* m_parent = nullptr;
	// Location of this node in file.
	unsigned m_line;

	// DataFile needs to access private members.
	friend class DataFile;
};

}}}
