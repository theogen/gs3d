#include <cstdio>

#include "data_node.hpp"

#include "util/str.hpp"

namespace tgamefw {
namespace loader {
namespace data {

// Dummy node.
const DataNode DataNode::m_dummy;

/* Constructors/operators */

DataNode::DataNode(const DataNode* parent) : m_parent(parent) {}

DataNode::DataNode(const DataNode& other) :
	m_children(other.m_children),
	m_tokens(other.m_tokens),
	m_parent(other.m_parent)
{}

DataNode& DataNode::operator=(const DataNode& other)
{
	m_tokens.clear();
	m_children.clear();

	m_children = other.m_children;
	m_tokens = other.m_tokens;
	m_parent = other.m_parent;

	return *this;
}


/* Tokens */

size_t DataNode::size() const
{
	return m_tokens.size();
}

const std::string& DataNode::token(int index, const std::string& fallback) const
{
	// Bounds checking
	if (index < 0 || (unsigned)index >= m_tokens.size())
		return fallback;

	return m_tokens[index];
}

double DataNode::value(int index, double fallback) const
{
	// Bounds checking
	if (index < 0 || (unsigned)index >= m_tokens.size())
		return fallback;

	std::string token = m_tokens[index];

	// Boolean
	if (util::Str::lower_ascii(token) == "true")
		return 1;

	// Color prefix to hex prefix
	if ('#' == token[0]) {
		token = token.erase(0, 1);
		token = "0x" + token;
	}

	// Hex number
	if (2 < token.size() && token[0] == '0' && token[1] == 'x')
		return util::Str::atoi(token, 16);

	// Binary number
	if (2 < token.size() && token[0] == '0' && token[1] == 'b') {
		token = token.erase(0, 2);
		return util::Str::atoi(token, 2);
	}

	// Decimal number
	return util::Str::atof(m_tokens[index]);
}

bool DataNode::is_num(int index) const
{
	// Bounds checking
	if (index < 0 || (unsigned)index >= m_tokens.size())
		return false;

	const std::string& token = m_tokens[index];
	size_t pos = 0;
	int decimal = 0;

	// Color prefix
	if ('#' == token[0])
		++pos;

	// Hex number
	if (2 < token.size() && token[0] == '0' && token[1] == 'x')
		pos += 2;

	// Binary number
	if (2 < token.size() && token[0] == '0' && token[1] == 'b')
		pos += 2;
	
	for (; pos < token.length(); ++pos) {
		char c = token[pos];
		if (c >= '0' && c <= '9')
			continue;
		if ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
			continue;
		if (c == '.') {
			if (decimal)
				return false; // More than 1 dot is forbidden
			decimal = true;
			continue;
		}
		return false;
	}
	return true;
}


/* Children */

bool DataNode::has_children() const
{
	return 0 != m_children.size();
}

const DataNode& DataNode::operator[](const std::string& first_token) const
{
	// Check if the token matches cached node.
	if (last_child != nullptr &&
		last_child->m_tokens.size() != 0 &&
		last_child->m_tokens[0] == first_token)
	{
		return *last_child;
	}

	// Searching for match through all children.
	for (const DataNode& child : m_children)
		if (0 != child.m_tokens.size() && child.m_tokens[0] == first_token)
			// Caching this child and returning it.
			return *(last_child = &child);

	// No match found, returning dummy.
	last_child = &DataNode::m_dummy;
	return DataNode::m_dummy;
}

std::list<DataNode>::const_iterator DataNode::begin() const
{
	return m_children.begin();
}

std::list<DataNode>::const_iterator DataNode::end() const
{
	return m_children.end();
}


/* Debugging */

#if 0
void DataNode::print_trace(int token, LogLevel level) const
{
	if (!glog.will_print(level))
		return;

	std::ostream& out = glog.get_output_stream(level);

	// Getting level
	std::vector<const DataNode*> nodes;
	const DataNode* node = this;
	while (node != nullptr) {
		nodes.push_back(node);
		node = node->m_parent;
	}

	// Last element is m_root, which we don't need.
	nodes.pop_back();

	// Printing
	for (size_t i = nodes.size(); i > 0; --i) {
		node = nodes[i - 1];
		// Indentation
		for (size_t j = 0; j < nodes.size() - i; ++j)
			out << ">   ";

		// Highlight node
		if (i == 1 && token == -1)
			out << AnsiSeq::bold << AnsiSeq::brown;

		// Node
		for (size_t j = 0; j < node->size(); ++j) {
			// Determining quotation mark type
			std::string quote = "";
			for (size_t k = 0; k < node->m_tokens[j].size(); ++k) {
				if (' ' == node->m_tokens[j][k])
					quote = '"';
				if ('"' == node->m_tokens[j][k]) {
					quote = '`';
					break;
				}
			}

			// Highlight this token
			if (i == 1 && (int)j == token)
				out << AnsiSeq::bold << AnsiSeq::brown;

			// Printing node
			out << quote << node->m_tokens[j] << quote << ' ';

			// End of highlight
			if (i == 1 && (int)j == token)
				out << AnsiSeq::reset;
		}

		// Newline
		out << '\n';
	}

	// End of highlight
	out << AnsiSeq::reset;
}
#endif

}}} // Namespace.
