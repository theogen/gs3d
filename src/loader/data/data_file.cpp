#include <stdexcept>
#include <cstring>
#include <fstream>
#include <vector>

#include "data_file.hpp"

namespace tgamefw {
namespace loader {
namespace data {

/* Constructors */

void DataFile::clear()
{
	m_root.m_children.clear();
	m_root.m_tokens.clear();
}

/* Loading */

bool DataFile::load(const std::string& path)
{
	std::ifstream file(&path[0]);
	if (file.fail()) {
		throw std::runtime_error(path + ": " + strerror(errno));
		return false;
	}
	m_filename = path;
	return load(file);
}

bool DataFile::load(std::istream& in)
{
	clear();

	std::vector<char> data;
	static const size_t block_size = 4096;

	while(in)
	{
		size_t current_size = data.size();
		data.resize(current_size + block_size);
		in.read(&*data.begin() + current_size, block_size);
		data.resize(current_size + in.gcount());
	}

	// Make sure the file always ends with a newline.
	if('\n' != data.back())
		data.push_back('\n');
	
	parse(&*data.begin(), &*data.end());

	return true;
}


/* Parsing */

// Parse from a null-terminated string.
void DataFile::parse(const char* data)
{
	clear();

	size_t len = strlen(data);
	parse(&data, data + len, 0, m_root);
}

// Parse from a memory region.
void DataFile::parse(const char* it, const char* end)
{
	clear();
	parse(&it, end, 0, m_root);
}

static inline bool
getlevel(const char* it, const char *end, const char** newit, int* level);

void
DataFile::parse(const char** it, const char* end, int level, DataNode& parent)
{
	DataNode node;
	int mylevel = 0;
	const char* newit;
	while (true) {
		// Getting indentation level.
		if (!getlevel(*it, end, &newit, &mylevel))
			break;

		// Parse deeper level.
		if (level < mylevel) {
			if (mylevel - level > 1) {
				std::string msg = m_filename + ": extra indentation level.\n";
				std::cout << msg;
			}
			parse(it, end, mylevel, parent.m_children.back());
			continue;
		}

		// Return if level is lower.
		else if (level > mylevel)
			return;

		// Apply new position.
		*it = newit;

		// Get tokens.
		if (!parse_node(it, end, node))
			break;

		// Assign to parent.
		node.m_parent = &parent;
		parent.m_children.push_back(node);

		// Clear for next pass.
		node.m_children.clear();
		node.m_tokens.clear();
		node.m_parent = nullptr;
	}
}

// Get tokens of a single node.
// This function assumes that whitespaces and comments were already skipped.
inline bool
DataFile::parse_node(const char** it, const char* end, DataNode& node)
{
	std::string token = "";
	// Can be 0 for none, '"' or '`'
	char quote = 0;

	for (; *it < end; ++(*it)) {
		char c = **it;

		// Quotes
		if ('"' == c && '`' != quote) {
			quote = quote ? 0 : '"';
			continue;
		}
		if ('`' == c && '"' != quote) {
			quote = quote ? 0 : '`';
			continue;
		}

		// DOS newlines
		if (c == '\r')
			continue;

		// Submit token
		if (c == '\n' || (!quote && (c == ' ' || c == '\t'))) {
			node.m_tokens.push_back(token);
			token = "";

			// Return if it's newline
			if (c == '\n') {
				if (quote)
					std::cout << "No terminating quotation mark\n";
				break;
			}

			continue;
		}

		token += c;
	}

	return *it < end || 0 < node.size();
}


/* Children */

std::list<DataNode>::const_iterator DataFile::begin() const
{
	return m_root.begin();
}

std::list<DataNode>::const_iterator DataFile::end() const
{
	return m_root.end();
}


/* Debugging */

void DataFile::print_contents() const
{
	print_contents(m_root, -1);
}

void DataFile::print_contents(const DataNode& node, int level) const
{
	if (0 < node.size()) {
		for (int i = 0; i < level; ++i)
			printf(">   ");
		for (size_t i = 0; i < node.size(); ++i) {
			printf("[%s] ", node.m_tokens[i].c_str());
		}
		printf("\n");
	}
	for (const DataNode& child : node) {
		print_contents(child, level + 1);
	}
}


/* Static functions */

// Skip blank lines and comments and get indentation level
static inline bool
getlevel(const char* it, const char* end, const char** newit, int* level)
{
	bool comment = false;

	for (*newit = it; *newit < end; ++(*newit)) {
		switch (**newit) {
			case '\t':
				if (!comment)
					++(*level);
				break;
			case '#':
				if (!comment)
					comment = true;
				break;
			case '\r':
				break;
			case '\n':
				*level = 0;
				comment = false;
				break;
			default:
				if (!comment)
					return true;
				break;
		}
	}

	return it < end;
}

}}} // Namespace.
