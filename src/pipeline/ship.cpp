#include <iostream>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "ship.hpp"

namespace tgamefw {
namespace pipeline {

Ship::Ship(const video::Display* display, Loader* loader)
	: Pipeline(display, loader)
{
	load_model();

	m_light_pos_uniform = bgfx::createUniform("light_position", bgfx::UniformType::Vec4, 1);
	m_light_color_uniform = bgfx::createUniform("light_color", bgfx::UniformType::Vec4, 1);

	bgfx::ShaderHandle vsh = load_shader("ship", "vs_ship.bin");
	bgfx::ShaderHandle fsh = load_shader("ship", "fs_ship.bin");
	m_program = bgfx::createProgram(vsh, fsh, true);
}

Ship::~Ship()
{
	bgfx::destroy(m_colors_uniform);
	bgfx::destroy(m_light_pos_uniform);
	bgfx::destroy(m_light_color_uniform);
	bgfx::destroy(m_ibh);
	bgfx::destroy(m_vbh);
	bgfx::destroy(m_program);
}

void Ship::load_model()
{
	OBJModel model = p_loader->load_obj_model("testproj/model/red_ship_2.obj", "testproj/model/red_ship_2.mtl");

	bgfx::VertexLayout layout;
	layout.begin()
		.add(bgfx::Attrib::Position,  3, bgfx::AttribType::Float)
		.add(bgfx::Attrib::Normal,    3, bgfx::AttribType::Float)
		.skip(sizeof(glm::vec2))
		.skip(sizeof(glm::vec3))
		.add(bgfx::Attrib::TexCoord0,  1, bgfx::AttribType::Float)
	.end();

	// Vertex buffer.
	m_vbh = bgfx::createVertexBuffer(
		bgfx::copy(model.vertices.data(), model.vertices.size() * sizeof(model.vertices[0])),
		layout
	);

	// Index buffer.
	m_ibh = bgfx::createIndexBuffer(
		bgfx::copy(model.indices.data(), model.indices.size() * sizeof(model.indices[0])),
		BGFX_BUFFER_INDEX32
	);

	for (const auto& material : model.materials) {
		m_colors.push_back({
			material.diffuse[0],
			material.diffuse[1],
			material.diffuse[2],
			1.0f
		});
	}

	m_colors_uniform = bgfx::createUniform("colors", bgfx::UniformType::Vec4, model.materials.size());
}

void Ship::render(scene::Camera* camera, std::vector<scene::Node*> nodes, scene::Light* light)
{
	//for (auto node : nodes)
	{
		auto node = nodes[0];
		auto transform_matrix = node->transform_matrix();
		bgfx::setTransform(&transform_matrix);

		// Set vertex and index buffer.
		bgfx::setVertexBuffer(0, m_vbh);
		bgfx::setIndexBuffer(m_ibh);

		// Uniform.
		bgfx::setUniform(m_colors_uniform, m_colors.data(), m_colors.size());
		auto light_pos = glm::vec4(light->position(), 1);
		auto light_color = glm::vec4(light->color(), 1);
		bgfx::setUniform(m_light_pos_uniform, &light_pos);
		bgfx::setUniform(m_light_color_uniform, &light_color);

		// Set render states.
		bgfx::setState(BGFX_STATE_DEFAULT | BGFX_STATE_MSAA);

		// Submit primitive for rendering to view 0.
		bgfx::submit(0, m_program);
	}
}

}}
