#pragma once

#include "../video/pipeline.hpp"

namespace tgamefw {
namespace pipeline {

class Ship : public video::Pipeline {
public:
	Ship(const video::Display* display, Loader* loader);
	virtual ~Ship();

	virtual void render(scene::Camera* camera, std::vector<scene::Node*> nodes, scene::Light* light) override;

private:
	void load_model();
	void update_transform_matrix();

private:
	bgfx::VertexBufferHandle m_vbh;
	bgfx::IndexBufferHandle m_ibh;
	bgfx::UniformHandle m_colors_uniform;
	bgfx::UniformHandle m_light_pos_uniform;
	bgfx::UniformHandle m_light_color_uniform;
	std::vector<glm::vec4> m_colors;

	bgfx::ProgramHandle m_program;
}; 

}}
