#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "cube.hpp"

namespace tgamefw {
namespace pipeline {

Cube::Cube(const video::Display* display, Loader* loader)
	: Pipeline(display, loader)
{
	load_model();
	load_texture();

	bgfx::ShaderHandle vsh = load_shader("cube", "vs_cube.bin");
	bgfx::ShaderHandle fsh = load_shader("cube", "fs_cube.bin");
	m_program = bgfx::createProgram(vsh, fsh, true);
}

Cube::~Cube()
{
	bgfx::destroy(m_program);
	bgfx::destroy(m_th);
	bgfx::destroy(m_tu);
	bgfx::destroy(m_ibh);
	bgfx::destroy(m_vbh);
}

void Cube::load_model()
{
	OBJModel model = p_loader->load_obj_model("testproj/model/cube.obj");

	bgfx::VertexLayout layout;
	layout.begin()
		.add(bgfx::Attrib::Position,  3, bgfx::AttribType::Float)
		.add(bgfx::Attrib::Normal,    3, bgfx::AttribType::Float)
		.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
		.skip(sizeof(glm::vec3) + sizeof(float))
	.end();

	// Vertex buffer.
	m_vbh = bgfx::createVertexBuffer(
		bgfx::copy(model.vertices.data(), model.vertices.size() * sizeof(model.vertices[0])),
		layout
	);

	// Index buffer.
	m_ibh = bgfx::createIndexBuffer(
		bgfx::copy(model.indices.data(), model.indices.size() * sizeof(model.indices[0])),
		BGFX_BUFFER_INDEX32
	);
}

void Cube::load_texture()
{
	ImageResource img = p_loader->load_image("testproj/box.png");
	m_th = bgfx::createTexture2D(
		img.width, img.height,
		false, // hasMips
		1,
		bgfx::TextureFormat::Enum::RGBA8,
		BGFX_SAMPLER_MIN_POINT | BGFX_SAMPLER_MAG_POINT,
		bgfx::copy(img.pixels, img.size)
	);

	m_tu = bgfx::createUniform("s_texColor", bgfx::UniformType::Sampler);

	p_loader->free_image(img);
}

void Cube::render(scene::Camera* camera, std::vector<scene::Node*> nodes, scene::Light* light)
{
	for (auto node : nodes)
	{
		auto transform_matrix = node->transform_matrix();
		bgfx::setTransform(&transform_matrix);

		// Set vertex and index buffer.
		bgfx::setVertexBuffer(0, m_vbh);
		bgfx::setIndexBuffer(m_ibh);

		// Texture.
		bgfx::setTexture(0, m_tu, m_th);

		// Set render states.
		bgfx::setState(BGFX_STATE_DEFAULT | BGFX_STATE_MSAA);

		// Submit primitive for rendering to view 0.
		bgfx::submit(0, m_program);
	}
}

}}
