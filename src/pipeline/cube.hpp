#pragma once

#include "../video/pipeline.hpp"

namespace tgamefw {
namespace pipeline {

class Cube : public video::Pipeline {
public:
	Cube(const video::Display* display, Loader* loader);
	virtual ~Cube();

	virtual void render(scene::Camera* camera, std::vector<scene::Node*> nodes, scene::Light* light) override;

private:
	void load_model();
	void load_texture();
	void update_transform_matrix();

private:
	bgfx::VertexBufferHandle m_vbh;
	bgfx::IndexBufferHandle m_ibh;

	bgfx::UniformHandle m_tu;
	bgfx::TextureHandle m_th;

	bgfx::ProgramHandle m_program;
}; 

}}
