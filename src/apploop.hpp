#pragma once

#include "event/event_collector.hpp"
#include "input/keyboard.hpp"

#include "video/display.hpp"

#include "loader/resource_manager.hpp"
#include "loader/loader.hpp"

#include "pipeline/cube.hpp"
#include "pipeline/ship.hpp"

namespace tgamefw {

class AppLoop {
public:
	AppLoop(bgfx::RendererType::Enum renderer);
	~AppLoop();

	void loop();

private:
	ResourceManager* p_resource_manager;
	Loader* p_loader;

	Keyboard* p_keyboard;
	EventCollector* p_event_collector;

	video::Display* p_display;

	pipeline::Cube* p_cube_pipeline;
	pipeline::Ship* p_ship_pipeline;
};

}
