alias shaderc=../lib/bgfx/.build/linux64_gcc/bin/shadercRelease
for profile in 'glsl'; do
	mkdir -p "$profile"
	while read shader type; do
	shaderc\
		-f "$shader.sc" \
		-o "$profile/$shader.bin" \
		--profile "120" \
		--platform linux \
		--type "$type" \
		--verbose \
		-i ../lib/bgfx/src
	done << END
		vs_simple vertex
		fs_simple fragment
END
done
