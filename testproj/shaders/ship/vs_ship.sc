$input a_position, a_normal, a_texcoord0
$output v_materialID, v_normal, v_lightDir

#include <bgfx_shader.sh>

uniform vec4 light_position;

void main()
{
	vec4 wpos = mul(u_model[0], vec4(a_position, 1.0));
    gl_Position = mul(u_viewProj, wpos);
	v_materialID = a_texcoord0;

	v_normal = normalize(mul(u_model[0], vec4(a_normal, 0.0)).xyz);
	v_lightDir = normalize((light_position - wpos).xyz);
}
