vec3  a_position   : POSITION;
vec3  a_normal     : NORMAL;
float a_texcoord0  : TEXCOORD0;

flat float v_materialID : TEXCOORD0;
vec3 v_normal : TEXCOORD1 = vec3(0.0, 0.0, 1.0);
vec3 v_lightDir : TEXCOORD2 = vec3(1.0, 1.0, 1.0);
