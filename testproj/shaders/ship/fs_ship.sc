$input v_materialID, v_normal, v_lightDir

#include <bgfx_shader.sh>

uniform vec4 colors[6];

uniform vec4 light_color;

void main()
{
	float ndotl = dot(v_normal, v_lightDir);
	float brightness = max(ndotl, 0.2);
	vec4 diffuse = mul(light_color, brightness);
	//vec4 diffuse = mul(vec4(1.0, 1.0, 1.0, 1.0), brightness);
	//vec4 diffuse = light_color;
	//vec4 diffuse = vec4(1.0, 1.0, 1.0, 1.0);
	gl_FragColor = mul(colors[uint(v_materialID)], diffuse);
	//gl_FragColor = colors[uint(v_materialID)];
}
