# vim: ft=python

import os
import shutil
import glob
import shutil 
import subprocess

# Program/library name.
progname = 'gs3d'

# SCons environment.
env = Environment()

#########################################################################
# OPTIONS ###############################################################
#########################################################################

AddOption(
    '--progname',
    dest='progname',
    nargs=1, type='string',
    action='store',
    metavar='name',
    default=progname,
    help='Build & install program by given name.'
)
progname = GetOption('progname')

AddOption(
    '--prefix',
    dest='prefix',
    nargs=1, type='string',
    action='store',
    metavar='DIR',
    default='install',
    help='Installation prefix.'
)

AddOption(
    '--build-prefix',
    dest='buildpfx',
    nargs=1, type='string',
    action='store',
    metavar='DIR',
    default='build',
    help='Build prefix.'
)

AddOption(
    '--verbose',
    dest='verbose',
    action='store_true',
    default=False,
    help='Verbose output.'
)

#########################################################################
# VERBOSITY #############################################################
#########################################################################

if not GetOption('verbose'):
    env.Append(
        CXXCOMSTR = "\033[32mCompiling `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        LINKCOMSTR = "\033[36mLinking `\033[39;1m$TARGET\033[36;21;24m'\033[0m",
        ARCOMSTR = "\033[32mArchiving `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        RANLIBCOMSTR = "\033[32mMaking library `\033[39;1m$TARGET\033[32;21;24m'\033[0m",
        INSTALLSTR = "\033[34mInstall `\033[39;1m$TARGET\033[34;21;24m'\033[0m",
        CLEANSTR = "\033[31mRemove `\033[39;1m$TARGET\033[31;21;24m'\033[0m"
    )


#########################################################################
# BUILD TYPE ############################################################
#########################################################################

# Build type.
if ARGUMENTS.get('build') != 'release':
    build_type = 'debug'
else:
    build_type = 'release'


#########################################################################
# DIRECTORIES ###########################################################
#########################################################################

src_dir = 'src'
lib_dir = 'thirdparty'

bx_repo = lib_dir + '/bx'
bx_inc  = bx_repo + '/include'

bgfx_repo = lib_dir + '/bgfx'
bgfx_inc = bgfx_repo + '/include'
bgfx_build = bgfx_repo + '/.build/linux64_gcc/bin'


# Build prefix.
build_pfx = GetOption('buildpfx')

# Adding build type.
build_dir = build_pfx + '/' + build_type

build_bin_dir = build_dir + '/bin'
build_lib_dir = build_dir + '/lib'
build_obj_dir = build_dir + '/obj'

target_path = build_bin_dir + '/' + progname

# Installation prefix.
install_pfx = GetOption('prefix')

install_inc_dir = install_pfx + '/include/' + progname
install_lib_dir = install_pfx + '/lib'

env.VariantDir(build_obj_dir, src_dir, duplicate=0)

# Including all subdirectories recursively.
sources = Glob(build_obj_dir + '/*.cpp')
for dir in [x[0] for x in os.walk(src_dir)]:
    wo_basedir = dir[len(src_dir):]
    if len(wo_basedir) == 0:
        continue
    sources += Glob(build_obj_dir + '/' + wo_basedir + '/*.cpp')

#########################################################################
# FLAGS #################################################################
#########################################################################

# Show all warnings.
flags  = ' -Wall'

# Build type.
if build_type == 'debug':
    flags += ' -g -rdynamic'
else:
    flags += ' -O3 -DNDEBUG'

# C11 standard.
flags += ' -std=c11'

# Source directory.
flags += ' -I' + src_dir

# Libraries directory.
flags += ' -isystem' + lib_dir

flags += ' -isystem' + bx_inc
flags += ' -isystem' + bgfx_inc
flags += ' -L ' + bgfx_build
#flags += ' -DBGFX_CONFIG_RENDERER_OPENGL=46'
#flags += '-DBGFX_CONFIG_MULTITHREADED=0'

# Colored output.
flags += ' -fdiagnostics-color'


#########################################################################
# LIBRARIES #############################################################
#########################################################################

# Libraries.
env.Append(LIBS = [
    'SDL2',
    'SDL2_image',
    'vulkan',
    'GL',
    'X11',
    'dl',
    'pthread',
    'rt',
    'z',
    'bgfx' + build_type.title(),
    'bimg' + build_type.title(),
    'bimg_decode' + build_type.title(),
    'bx' + build_type.title(),
])


#########################################################################
# BUILD #################################################################
#########################################################################

def build_shader(directory:str, target:int):
    cmd = [
        'make',
        '-s',
        '-C', directory,  # Working directory
        'TARGET=%d' % target
    ]
    p = subprocess.Popen(cmd)
    p.wait()
    if (p.returncode != 0):
        exit(p.returncode)

for directory in [ 'cube', 'ship' ]:
    build_shader('testproj/shaders/' + directory, 4)
    build_shader('testproj/shaders/' + directory, 7)

target = env.Program(target_path, sources, parse_flags = flags)
